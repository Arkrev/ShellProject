#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <errno.h>


#define ARRAY_COUNT(arr) sizeof(arr) / sizeof((arr)[0])

// Simplifed xv6 shell.

#define MAXARGS 10
#define MAXBGPROCS 100

pid_t bgprocs[MAXBGPROCS] = {};

// All commands have at least a type. Have looked at the type, the code
// typically casts the *cmd to some specific cmd type.
struct cmd {
    int type;          //  ' ' (exec), | (pipe), '<' or '>' for redirection
};

struct execcmd {
    int type;              // ' '
    char *argv[MAXARGS];   // arguments to the command to be exec-ed
};

struct redircmd {
    int type;          // < or > 
    struct cmd *cmd;   // the command to be run (e.g., an execcmd)
    char *file;        // the input/output file
    int mode;          // the mode to open the file with
    int fd;            // the file descriptor number to use for the file
};

struct pipecmd {
    int type;          // |
    struct cmd *left;  // left side of pipe
    struct cmd *right; // right side of pipe
};

int fork1(void);  // Fork but exits on failure.
struct cmd *parsecmd(char*);

// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>";

void cmderr(char *cmdname)
{
	switch(errno)
	{
		case ENOENT:
		{
			fprintf(stderr, "sh: %s: not found\n", cmdname);
			break;
		}
		default:
		{
			perror(cmdname);
			break;
		}
	}
}

// Execute cmd.  Never returns.
void runcmd(struct cmd *cmd)
{
    struct execcmd *ecmd;
    struct pipecmd *pcmd;
    struct redircmd *rcmd;

    if(cmd == 0)
        exit(0);

    switch(cmd->type){
        default:
		{
            fprintf(stderr, "unknown runcmd\n");
            exit(-1);
		}

        case ' ':
		{
            ecmd = (struct execcmd*)cmd;
            if(ecmd->argv[0] == 0)
                exit(0);

			execvp(ecmd->argv[0], ecmd->argv);
			cmderr(ecmd->argv[0]);
			
            break;
		}

        case '>':
        case '<':
		{
            rcmd = (struct redircmd*)cmd;

			// Default permissions: rw-r--r--.
			int file = open(rcmd->file, rcmd->mode,
							S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

			if (file == -1)
			{
				perror("redirection");
				break;
			}
			
			int status = rcmd->fd ? dup2(file, STDOUT_FILENO) : dup2(file, STDIN_FILENO);

			if (status == -1)
			{
				perror("dup2");
				break;
			}
			
			if (close(file) == -1)
			{
				perror("close");
				break;
			}
			
            runcmd(rcmd->cmd);
            break;
		}

        case '|':
		{
            pcmd = (struct pipecmd*)cmd;
			
			int pipefd[2];

			if (pipe(pipefd) != 0)
			{
				perror("pipe");
				break;
			}

			pid_t child1, child2;
			
			switch(child1 = fork1())
			{
				case 0:
				{
					close(pipefd[1]);
					
					if(dup2(pipefd[0], STDIN_FILENO) == -1)
					{
						perror("dup2");
						exit(0);
					}
					if(close(pipefd[0]) == -1)
					{
						perror("close pipe");
						exit(0);
					}

					runcmd(pcmd->right);
					break;
				}
				
				default: break;
			}

			switch(child2 = fork1())
			{
				case 0:
				{
					if(close(pipefd[0]) == -1)
					{
						perror("close pipe");
						exit(0);
					}
					if(dup2(pipefd[1], STDOUT_FILENO) == -1)
					{
						perror("dup2");
						exit(0);
					}


					if(close(pipefd[1]) == -1){
						perror("close pipe");
						exit(0);
					}
					
					runcmd(pcmd->left);
				}
				case -1: break;

				default:
				{
					close(pipefd[0]);
					close(pipefd[1]);

					wait(NULL);
					wait(NULL);
					break;
				}
			}
			
			break;
		}
	}
	
    exit(0);
}

int getcmd(char *buf, int nbuf)
{
	printf("$ ");
    memset(buf, 0, nbuf);
    fgets(buf, nbuf, stdin);
    if(buf[0] == 0) // EOF
        return -1;
    return 0;
}

void stopbgprocs()
{
	for (int i = 0; i < ARRAY_COUNT(bgprocs); ++i)
	{
		pid_t pid = bgprocs[i];
		
		if (!pid)
		{
			continue;
		}
			
		if (waitpid(pid, NULL, WNOHANG) != pid)
		{
			kill(pid, SIGHUP);
			bgprocs[i] = 0;
		}
	}
	
	exit(0);
}

void hand(int signal)
{
	switch (signal)
	{
		case SIGINT:
		{
			printf("\n$ ");
			fflush(stdin);
			fflush(stdout);
		}
		case SIGCHLD:
		{
			pid_t pid = waitpid(-1, NULL, WNOHANG);
			
			if (pid == -1)
			{
				break;
			}
			
			for (int i = 0; i < ARRAY_COUNT(bgprocs); ++i)
			{
				if (pid == bgprocs[i])
				{
					bgprocs[i] = 0;
					break;
				}
			}
			break;
		}

		case SIGHUP:
		{
			stopbgprocs();
			exit(0);
		}
		default:
		{
			break;
		}
	}
}
	
int main(void)
{
	int childList[100];
    static char buf[100];

	signal(SIGINT,	 &hand);
	signal(SIGQUIT,	 &hand);
	signal(SIGTERM,	 &hand);
	signal(SIGCHLD,	 &hand);
	signal(SIGHUP,	 &hand);

	// Read and run input commands.
	while(getcmd(buf, sizeof(buf)) >= 0){
		unsigned long indlstchr = strlen(buf) - 1;
		
		if(buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' '){
			buf[indlstchr] = 0;  // chop \n
			if(chdir(buf+3) < 0)
				fprintf(stderr, "cannot cd %s\n", buf+3);
			continue;
		}

		char *sbgcs = strchr(buf, '&');
		int bg = 0;
		
		if (sbgcs != NULL)
		{
			char *bgcs = sbgcs + 1;
			
			while (((bgcs - buf) <= indlstchr) &&
				   strchr(whitespace, *bgcs))
			{
				++bgcs;
			}

			if ((bgcs - buf) > indlstchr)
			{
				*sbgcs = '\0';
				bg = 1;
			}
			else
			{
				fprintf(stderr, "Syntax error: \"%c\" unexpected\n", *bgcs);
				continue;
			}
		}

		pid_t child = fork1();
		
		if(child == 0)
		{
			setpgid(0, 0); // In case this path is run first.
			
			runcmd(parsecmd(buf));
		}
		else
		{
			setpgid(child, child);
			
			if (!bg)
			{
				signal(SIGTTOU, SIG_IGN);
				
				tcsetpgrp(STDIN_FILENO, child); // Make child the foreground process.

				waitpid(child, NULL, 0);
				
				tcsetpgrp(STDIN_FILENO, getpgid(getpid()));
			}
			else
			{
				int i = 0;
				while(bgprocs[i])
				{
					++i;
				}

				if (i < ARRAY_COUNT(bgprocs))
				{
					bgprocs[i] = child;
				}
			}
		}
	}

	stopbgprocs();
	exit(0);
}

int fork1(void)
{
    int pid;

    pid = fork();
    if(pid == -1)
        perror("fork");
    return pid;
}

struct cmd* execcmd(void)
{
    struct execcmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = ' ';
    return (struct cmd*)cmd;
}

struct cmd* redircmd(struct cmd *subcmd, char *file, int type)
{
    struct redircmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = type;
    cmd->cmd = subcmd;
    cmd->file = file;
    cmd->mode = (type == '<') ?  O_RDONLY : O_WRONLY|O_CREAT|O_TRUNC;
    cmd->fd = (type == '<') ? 0 : 1;
    return (struct cmd*)cmd;
}

struct cmd* pipecmd(struct cmd *left, struct cmd *right)
{
    struct pipecmd *cmd;

    cmd = malloc(sizeof(*cmd));
    memset(cmd, 0, sizeof(*cmd));
    cmd->type = '|';
    cmd->left = left;
    cmd->right = right;
    return (struct cmd*)cmd;
}

int gettoken(char **ps, char *es, char **q, char **eq)
{
    char *s;
    int ret;

    s = *ps;
    while(s < es && strchr(whitespace, *s))
        s++;
    if(q)
        *q = s;
    ret = *s;
    switch(*s){
        case 0:
            break;
        case '|':
        case '<':
            s++;
            break;
        case '>':
            s++;
            break;
        default:
            ret = 'a';
            while(s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
                s++;
            break;
    }
    if(eq)
        *eq = s;

    while(s < es && strchr(whitespace, *s))
        s++;
    *ps = s;
    return ret;
}

// ps : pointer to a string
// es : pointer to the end of the string
// moves *ps to the next character, returns true if this character is in the
// string toks
int peek(char **ps, char *es, char *toks)
{
    char *s;

    s = *ps;
    while(s < es && strchr(whitespace, *s))
        s++;
    *ps = s;
    return *s && strchr(toks, *s);
}

struct cmd *parseline(char**, char*);
struct cmd *parsepipe(char**, char*);
struct cmd *parseexec(char**, char*);

// make a copy of the characters in the input buffer, starting from s through es.
// null-terminate the copy to make it a string.
char *mkcopy(char *s, char *es)
{
    int n = es - s;
    char *c = malloc(n+1);
    assert(c);
    strncpy(c, s, n);
    c[n] = 0;
    return c;
}

struct cmd* parsecmd(char *s)
{
    char *es;
    struct cmd *cmd;

    es = s + strlen(s);
    cmd = parseline(&s, es);
    peek(&s, es, "");
    if(s != es){
        fprintf(stderr, "leftovers: %s\n", s);
        exit(-1);
    }
    return cmd;
}

struct cmd* parseline(char **ps, char *es)
{
    struct cmd *cmd;
    cmd = parsepipe(ps, es);
    return cmd;
}

struct cmd* parsepipe(char **ps, char *es)
{
    struct cmd *cmd;

    cmd = parseexec(ps, es);
    if(peek(ps, es, "|")){
        gettoken(ps, es, 0, 0);
        cmd = pipecmd(cmd, parsepipe(ps, es));
    }
    return cmd;
}

struct cmd* parseredirs(struct cmd *cmd, char **ps, char *es)
{
    int tok;
    char *q, *eq;

    while(peek(ps, es, "<>")){
        tok = gettoken(ps, es, 0, 0);
        if(gettoken(ps, es, &q, &eq) != 'a') {
            fprintf(stderr, "missing file for redirection\n");
            exit(-1);
        }
        switch(tok){
            case '<':
                cmd = redircmd(cmd, mkcopy(q, eq), '<');
                break;
            case '>':
                cmd = redircmd(cmd, mkcopy(q, eq), '>');
                break;
        }
    }
    return cmd;
}

struct cmd* parseexec(char **ps, char *es)
{
    char *q, *eq;
    int tok, argc;
    struct execcmd *cmd;
    struct cmd *ret;

    ret = execcmd();
    cmd = (struct execcmd*)ret;

    argc = 0;
    ret = parseredirs(ret, ps, es);
    while(!peek(ps, es, "|")){
        if((tok=gettoken(ps, es, &q, &eq)) == 0)
            break;
        if(tok != 'a') {
            fprintf(stderr, "syntax error\n");
            exit(-1);
        }
        cmd->argv[argc] = mkcopy(q, eq);
        argc++;
        if(argc >= MAXARGS) {
            fprintf(stderr, "too many args\n");
            exit(-1);
        }
        ret = parseredirs(ret, ps, es);
    }
    cmd->argv[argc] = 0;
    return ret;
}
